export interface AuthModel {
    username: string;
    status: boolean;
    message: string;
    jwt: string;
    authenticated: boolean;
}
