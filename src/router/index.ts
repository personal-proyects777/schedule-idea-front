import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import publicRoutes from '@/router/publicRoutes.ts';
import privateRoutes from '@/router/privateRoutes.ts';
// import { useAuthStore } from '@/store/authStore.ts';

const routes: RouteRecordRaw[] = [...publicRoutes, ...privateRoutes];

const router = createRouter({
    history: createWebHistory(import.meta.env.VITE_BASE_URL),
    routes,
});
router.beforeEach((to, _from, next) => {
    const isProtected = to.matched.some((record) => record.meta.authenticated);
    // const authStore = useAuthStore();
    // const isAuthenticated = authStore.dataAuth.authenticated;
    const isAuthenticated = true;
    if (to.path === '/login' && isAuthenticated) {
        next('/dashboard');
    } else if (isProtected && !isAuthenticated) {
        next('/login');
    } else {
        next();
    }
});

export default router;
