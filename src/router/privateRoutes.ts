import { RouteRecordRaw } from 'vue-router';
import DashboardPage from '@/pages/private/DashboardPage.vue';

const privateRoutes: RouteRecordRaw[] = [
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: DashboardPage,
        meta: {
            authenticated: true,
        },
    },
];

export default privateRoutes;
