import { RouteRecordRaw } from 'vue-router';
import LoginPage from '@/pages/auth/LoginPage.vue';
import HomePage from '@/pages/public/HomePage.vue';

const publicRoutes: RouteRecordRaw[] = [
    {
        path: '/',
        name: 'Home',
        component: HomePage,
        meta: {
            authenticated: false,
        },
    },
    {
        path: '/login',
        name: 'Login',
        component: LoginPage,
        meta: {
            authenticated: false,
        },
    },
];

export default publicRoutes;
