import http from '../config.ts';

const authenticate = async <T>(username: string, password: string): Promise<T> => {
    const response = await http.post<T>(
        '/auth/login',
        {
            username,
            password,
        },
        { withCredentials: true },
    );
    return response.data;
};
export default {
    authenticate,
};
