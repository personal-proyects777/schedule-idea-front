import axios, { AxiosInstance } from 'axios';

const instance: AxiosInstance = axios.create({
    baseURL: import.meta.env.VITE_API_ENDPOINT,
    headers: {
        'Content-Type': 'application/json',
    },
});
instance.interceptors.response.use(undefined, (error) => {
    const { data } = error.response;
    throw {
        code: data.code,
        message: data.message,
        details: data.details,
        time: data.time,
    };
});

export default instance;
