import ScheduleController from './schedule/scheduleService.ts';
import AuthenticateController from './auth/authService.ts';

export const API = {
    schedule: ScheduleController,
};

export const AUTH_API = {
    authService: AuthenticateController,
};
