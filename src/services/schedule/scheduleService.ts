import http from '../config.ts';
import { Token } from '@/utils';

const token = Token.getTokenFromLocalStorage();
if (token != undefined) {
    http.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}

async function getAll<T>(): Promise<T> {
    return (await http.get<T>('/api/v1/schedule')).data;
}

export default {
    getAll,
};
