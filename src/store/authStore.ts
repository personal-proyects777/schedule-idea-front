import { defineStore } from 'pinia';
import { AUTH_API } from '@/services';
import { ref } from 'vue';
import { AuthModel } from '@/model/AuthModel.ts';
import { isCustomError } from '@/utils/customErrorCheckType.ts';
import { Cookies, Token } from '@/utils';

export const useAuthStore = defineStore('auth', () => {
    const dataAuth = ref<AuthModel>({
        username: '',
        status: false,
        message: '',
        jwt: '',
        authenticated: false,
    });

    const token = Cookies.getCookieValue('auth_token');
    if (token != undefined) {
        dataAuth.value.jwt = token;
        dataAuth.value.authenticated = true;
    }

// Token.deleteTokenFromLocalStorage(); //only for testing
    const tokenLS = Token.getTokenFromLocalStorage();
    if (tokenLS != undefined) {
        dataAuth.value.jwt = tokenLS;
        dataAuth.value.authenticated = true;
    }

    const login = async (email: string, password: string) => {
        Cookies.cleanCookie('auth_token');
        Token.deleteTokenFromLocalStorage();
        try {
            const data = await AUTH_API.authService.authenticate<AuthModel>(email, password);
            dataAuth.value.username = data.username;
            dataAuth.value.message = data.message;
            dataAuth.value.status = data.status;
            dataAuth.value.jwt = data.jwt;
            dataAuth.value.authenticated = true;
            Token.saveTokenToLocalStorage(dataAuth.value.jwt);
        } catch (e) {
            if (isCustomError(e)) {
                dataAuth.value.message = e.message;
                dataAuth.value.status = false;
                alert(e.message);
                console.log(e);
            } else {
                alert('An unexpected error occurred');
                console.error(e);
            }
        }
    };

    return {
        dataAuth,
        login,
    };
});
