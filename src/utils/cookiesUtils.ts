const getCookieValue = (cookieName: string): string | undefined => {
    const cookies = document.cookie.split(';');
    for (const cookie of cookies) {
        const [name, value] = cookie.split('=').map((c) => c.trim());
        if (name === cookieName) {
            return decodeURIComponent(value);
        }
    }
    return undefined;
};

const cleanCookie = (cookieName: string): void => {
    document.cookie = `${cookieName}=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/`;
};

export default {
    getCookieValue,
    cleanCookie,
};
