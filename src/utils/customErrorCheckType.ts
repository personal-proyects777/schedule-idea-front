// Define a type guard to check if the error is of the expected type
export const isCustomError = (error: any): error is { code: string; message: string; details: any; time: string } => {
    return error && typeof error.message === 'string';
};
