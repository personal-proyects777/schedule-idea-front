const saveTokenToLocalStorage = (token: string): void => {
    localStorage.setItem('auth_token', token);
};

const getTokenFromLocalStorage = (): string | null => {
    return localStorage.getItem('auth_token');
};

const deleteTokenFromLocalStorage = (): void => {
    localStorage.removeItem('auth_token');
};

export default {
    saveTokenToLocalStorage,
    getTokenFromLocalStorage,
    deleteTokenFromLocalStorage,
};
